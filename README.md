Quem Somos
''A Rimasil está a mais de 14 Anos no mercado, vendendo os melhores produtos com os melhores preços, sempre prezando pela ótima satisfação de nossos clientes e amigos, somos campeões de lentes digitais no norte do Paraná, nossa loja física é a maior de toda a região em espaço físico, e temos uma vasta gama de marcas de óculos de grau, óculos de sol, relógios, bolsas, e semi joias. 

Preço justo, qualidade e confiança, é tudo o que de mais valioso transmitimos aos nossos clientes. A ótica e relojoaria Rimasil, vem sempre preocurando superar os desafios e as dificuldades propostas pelo mercado, para assim garantir a total satisfação de nossos queridos clientes.  

-Acreditamos que qualidade gera Fidelidade!''



A Rimasil foi fundada em Maio de 2005 por Marcio J. F. Pirota, e Silvia F. B. Pirota, Marcio era um rapaz sonhador que sempre teve o sonho de ter seu próprio negócio, sempre teve um espírito empreendedor, na época ele trabalhava em uma relojoaria de um senhor de origem japonesa, onde ele aprendeu algumas de suas profissões, eles venderam uma casa que tinha para montar a loja em um salão de duas portas que haviam alugado, bem no início entrou em média 5 sujeitos durante uma noite e roubaram boa parte da mercadoria mas eles ficaram em paz, por que Deus já havia falado que ia permitir uma grande prova na vida deles. Na próxima semana eles reabriram a loja com pouquíssima mercadoria uma longe da outra exposta, e Deus abençoou tanto que em pouco tempo ele já havia faturado mais que seu antigo patrão, e a empresa foi crescendo e crescendo até chegar no que está hoje.

Rimasil à alguns anos atrás...


  Missão: Mudar a vida dos brasileiros, trazendo mais luxo, conforto e estética com produtos de marcas renomadas 100% originais com Nota fiscal e de extrema qualidade à nível Brasil. Atender nossos clientes da melhor forma possível e tentar deixá-los(as) totalmente satisfeitos, ajudar o país a crescer e gerar riqueza para a população, ser ponto de referência de moda e estilo. Queremos trazer não mais um modo de comprar e sim uma experiência de vida. 
  Visão: Atualmente o maior culpado pelos desempregos no páis na nossa visão é por conta da China, que tem mão de obra escrava muita das vezes, e exportaram a nível mundial, no Brasil por exemplo qualquer pessoa deixa de comprar do comércio brasileiro e da indústria brasileira para comprar uma réplica produzida na China, podutos piratas sem nota fiscal, que além de não geragem um retorno para o Estado também não gera emprego, por que o empregador não consegue vender o suficiente para empregar as pessoas. Sempre lutamos para fazer um Brasil melhor e mais rico!
  Valores: Prezamos pelo bom atendimento em múltiplos canais para deixar nossos clientes satisfeitos. Não prezamos pela venda e sim pela satisfação dos nossos clientes, a Rimasil existe por que Deus nos abençoou e abriu essa grande porta nas nossas vidas.


  Curiosidade sobre a Logotipo:

A logo da Rimasil foi criada também por Marcio J. F. Pirota, muitos não sabem o por quê do nome, mas sua verdadeira origem é que ele se inspirou nos nomes da sua família para criá-la, com o começo dos nomes dos 3 integrantes da família na época, RI - MA -SIL.    RI: RYan (filho), MA: MArcio (Marido) e SIL: SILvia (Esposa).

 
_______________________________________________________________________________________________________________________________________

Atendimento

(44)3636-1848

contato@oticarimasil.com.br
https://www.oticarimasil.com.br/